package com.nike.demo.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureWebClient;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.ExpectedCount;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.client.match.MockRestRequestMatchers;
import org.springframework.test.web.client.response.MockRestResponseCreators;

import com.nike.demo.config.Config;
import com.nike.demo.config.ShoesList;
import com.nike.demo.dao.ShoesDao;
import com.nike.demo.exception.NikeException;
import com.nike.demo.model.Shoes;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RunWith(SpringRunner.class)
@RestClientTest(ShoesDao.class)
@AutoConfigureWebClient(registerRestTemplate = true)
public class DaoTests {

  @MockBean
  Config config;

  @MockBean
  ShoesList list;

  @Autowired
  private MockRestServiceServer server;

  @Autowired
  private ShoesDao dao;

  @Before
  public void setup() {
    Mockito.when(config.getNodeUrl()).thenReturn("/fetch-random-price/%s");

    list.getList();
    List<Shoes> shoes = new ArrayList<>();
    shoes.add(new Shoes());
    Mockito.when(list.getList()).thenReturn(shoes);
  }

  @Test
  public void testRandomDiscount() {

    server.expect(ExpectedCount.manyTimes(), MockRestRequestMatchers.requestTo("/fetch-random-price/1"))
        .andExpect(MockRestRequestMatchers.method(HttpMethod.GET))
        .andRespond(MockRestResponseCreators.withSuccess("\"shoePrice\":123}", MediaType.APPLICATION_JSON));

    String json = dao.getRandomeDiscount("1");
    System.out.println(json);
    assertNotNull(json);
  }

  @Test
  public void testgetOriginalPrices() {

    List<Shoes> shoes = dao.getOriginalPrices();
    assertNotEquals(0, shoes.size());
  }

  @Test
  public void negativeTest() {
    List<Shoes> shoes = dao.getOriginalPrices();
    try {
      if (shoes.size() == 1)
        throw new NikeException("Negative test");

    } catch (NikeException e) {
      log.info("Negative test");
    }
  }
}

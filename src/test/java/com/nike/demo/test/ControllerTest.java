package com.nike.demo.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureWebClient;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.nike.demo.controller.Controller;
import com.nike.demo.dto.DiscountShoes;
import com.nike.demo.service.ShoesService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RunWith(SpringRunner.class)
@RestClientTest(Controller.class)
@AutoConfigureWebClient(registerRestTemplate = true)
public class ControllerTest {

  @MockBean
  private ShoesService service;

  @Autowired
  private Controller controller;

  @Before
  public void setUpService() {

    Mockito.when(service.getRandomDiscount("1")).thenReturn("{\"shoePrice\":123}");
//    Mockito.when(controller.getShoePrice("1")).thenReturn("{\"shoePrice\":123}");

    List<DiscountShoes> list = new ArrayList<>();
    DiscountShoes s = new DiscountShoes();
    list.add(s);
    Mockito.when(service.getFlatDiscountShoes()).thenReturn(list);

    log.info("Set up shoes service done!");
  }

  @Test
  public void testRandomDiscount() {
    log.info("test random controller!");
    String result = controller.getShoePrice("1");
    System.out.println(result);
    assertNotNull(result);
  }

  @Test
  public void testFlatDiscount() {
    log.info("test flat discount controller!");
    List<DiscountShoes> list = controller.getFlatDiscount();
    assertEquals(list.size(), 1);
  }
}

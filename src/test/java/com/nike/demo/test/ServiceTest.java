package com.nike.demo.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.nike.demo.config.Config;
import com.nike.demo.config.ShoesList;
import com.nike.demo.dao.ShoesDao;
import com.nike.demo.dto.DiscountShoes;
import com.nike.demo.model.Shoes;
import com.nike.demo.service.ShoesService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RunWith(SpringRunner.class)
@RestClientTest(ShoesService.class)
public class ServiceTest {

  @MockBean
  Config config;

  @MockBean
  ShoesList list;

  @MockBean
  private ShoesDao dao;

  @Autowired
  private ShoesService service;

  @Before
  public void setUpService() {

    // mock to return price for id 1
    Mockito.when(dao.getRandomeDiscount("1")).thenReturn("{\"shoePrice\":123}");

    List<Shoes> list = new ArrayList<>();
    Shoes s = new Shoes();
    s.setMaxPrice(190.0);
    s.setMinPrice(5.0);
    s.setId(1);
    s.setModel("Test");

    list.add(s);
    // mock to return a pair of shoes
    Mockito.when(dao.getOriginalPrices()).thenReturn(list);

    // mock to set flat discount to 10
    Mockito.when(config.getFlatDiscount()).thenReturn(0.1);

    log.info("Set up shoes service done!");
  }

  @Test
  public void testRandomDiscount() {
    log.info("test random controller!");
    String result = service.getRandomDiscount("1");
    System.out.println(result);
    assertNotNull(result);
  }

  @Test
  public void testFlatDiscount() {
    log.info("test flat discount controller!");

    try {
      Method postConstruct = ShoesService.class.getDeclaredMethod("init", (Class<?>[]) null);
      postConstruct.invoke(service);
    } catch (Exception e) {
      log.error("Failed to invoke init method!", e);
    }

    List<DiscountShoes> list = service.getFlatDiscountShoes();
    assertEquals(list.size(), 1);

    // mock a pair of shoes with original price 190, mock flat discount is 10,
    // so the discounted price should be 171.00
    assertEquals(list.get(0).getDiscounted(), "171.00");
  }
}

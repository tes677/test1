package com.nike.demo.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import com.nike.demo.config.Config;
import com.nike.demo.config.ShoesList;
import com.nike.demo.model.Shoes;

@Repository
public class ShoesDao {

  @Autowired
  private Config config;

  @Autowired
  private ShoesList list;

  @Autowired
  private RestTemplate restTemplate;

  /**
   * call node.js server to get random price for specified shoes id
   * 
   * @param id
   * @return
   */
  public String getRandomeDiscount(String id) {
    String pattern = config.getNodeUrl();
    String url = String.format(pattern, id);

    return getByUrl(url);
  }

  public String getByUrl(String url) {
    return restTemplate.getForObject(url, String.class);
  }

  /**
   * get original prices the price of each shoes are configured in the
   * application.yml file currently if this implementation is change in future,
   * can simply change this method to adopt new API
   * 
   * @return original prices
   */
  public List<Shoes> getOriginalPrices() {
    return list.getList();
  }
}

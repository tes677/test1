package com.nike.demo.dto;

import lombok.Data;

@Data
public class DiscountShoes {

  private int id;
  private String model;
  private String original;
  private String discounted;
}

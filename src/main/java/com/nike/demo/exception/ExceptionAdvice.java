package com.nike.demo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestControllerAdvice
public class ExceptionAdvice {

  @ExceptionHandler(NikeException.class)
  @ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
  public ExceptionStatus processException(NikeException e) {
    ExceptionStatus status = new ExceptionStatus();
    status.setHttpStatus(HttpStatus.INTERNAL_SERVER_ERROR);
    status.message = e.getMessage();
    status.exceptionClass = e.getClass().getName();

    log.debug("Handle exception{class={}, message={}}", e.getClass().getName(), e.getMessage());
    return status;
  }

  @ExceptionHandler(Throwable.class)
  @ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
  public ExceptionStatus processException(Throwable e) {
    ExceptionStatus status = new ExceptionStatus();
    status.setHttpStatus(HttpStatus.INTERNAL_SERVER_ERROR);
    status.message = e.getMessage();
    status.exceptionClass = e.getClass().getName();

    log.debug("Handle exception{class={}, message={}}", e.getClass().getName(), e.getMessage());
    return status;
  }

  public static class ExceptionStatus {

    private int code;

    @JsonProperty("reason-phrase")
    private String reasonPhrase;

    private String message;

    @JsonProperty("exception-class")
    private String exceptionClass;

    /**
     * @return the code of HTTP status
     */
    public int getCode() {
      return code;
    }

    /**
     * @return the reason phrase of HTTP status
     */
    public String getReasonPhrase() {
      return reasonPhrase;
    }

    public void setHttpStatus(HttpStatus status) {
      this.code = status.value();
      this.reasonPhrase = status.getReasonPhrase();
    }

    /**
     * @return the message
     */
    public String getMessage() {
      return message;
    }

    /**
     * @return the class name of exception
     */
    public String getExceptionClass() {
      return exceptionClass;
    }
  }
}

package com.nike.demo.exception;

public class NikeException extends Exception {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  public NikeException(String msg) {
    super(msg);
  }

  public NikeException(String msg, Throwable t) {
    super(msg, t);
  }
}

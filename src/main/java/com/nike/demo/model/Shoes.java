package com.nike.demo.model;

import lombok.Data;

@Data
public class Shoes {
  private int id;
  private String model;
  private Double maxPrice;
  private Double minPrice;
}

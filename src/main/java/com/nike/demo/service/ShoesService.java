package com.nike.demo.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nike.demo.config.Config;
import com.nike.demo.dao.ShoesDao;
import com.nike.demo.dto.DiscountShoes;
import com.nike.demo.model.Shoes;

import jakarta.annotation.PostConstruct;

@Service
public class ShoesService {

  @Autowired
  private Config config;

  @Autowired
  private ShoesDao dao;

  private BigDecimal rate;

  @PostConstruct
  public void init() {
    rate = BigDecimal.valueOf(1.0 - config.getFlatDiscount());
  }

  /**
   * get flat discount shoes at a rate, which is initialized by a configuration in
   * application.yml file, and can be modified by API
   * 
   * @return the list of discounted shoes
   */
  public List<DiscountShoes> getFlatDiscountShoes() {
    List<Shoes> tmp = dao.getOriginalPrices();
    List<DiscountShoes> discounts = new ArrayList<>(tmp.size());

    tmp.forEach(s -> {
      DiscountShoes dto = new DiscountShoes();
      dto.setId(s.getId());
      dto.setModel(s.getModel());
      BigDecimal max = BigDecimal.valueOf(s.getMaxPrice());
      BigDecimal d = max.multiply(rate);

      dto.setOriginal(max.setScale(2, java.math.RoundingMode.HALF_UP).toPlainString());

      dto.setDiscounted(d.setScale(2, java.math.RoundingMode.HALF_UP).toPlainString());

      discounts.add(dto);
    });

    return discounts;
  }

  /**
   * get random price from node.js url
   * 
   * @param id
   * @return
   */
  public String getRandomDiscount(String id) {
    return dao.getRandomeDiscount(id);
  }
}

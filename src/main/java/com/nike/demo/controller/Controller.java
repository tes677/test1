package com.nike.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.nike.demo.config.Constrants;
import com.nike.demo.dto.DiscountShoes;
import com.nike.demo.service.ShoesService;

@RestController
@RequestMapping(Constrants.API)
public class Controller {

  @Autowired
  private ShoesService service;

  @ResponseBody
  @GetMapping("/shoe-price/{id}")
  public String getShoePrice(@PathVariable String id) {
    return service.getRandomDiscount(id);
  }

  @ResponseBody
  @GetMapping("/flat-discount")
  public List<DiscountShoes> getFlatDiscount() {
    return service.getFlatDiscountShoes();
  }
}

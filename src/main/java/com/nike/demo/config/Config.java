package com.nike.demo.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import lombok.Getter;

@Getter
@Configuration
public class Config {

  @Value("${node.js.url}")
  private String nodeUrl;

  @Value("${flat-discount}")
  private Double flatDiscount;
}

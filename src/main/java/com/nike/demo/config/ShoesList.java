package com.nike.demo.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import com.nike.demo.model.Shoes;

import lombok.Data;

@Data
@Component
@ConfigurationProperties(prefix = "shoes")
public class ShoesList {
  List<Shoes> list = new ArrayList<>();
}

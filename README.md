### Nike Interview Backend Code Challenge [Java]

_The provided code document should contain more details._

This project uses Maven & Spring Boot (Web Starter Pack). So you will need to install `java (> 1.8)` & `maven` on your machine first.
For more information, you can visit the [Spring Boot docs](https://docs.spring.io/spring-boot/docs/current/reference/html/getting-started.html#getting-started.introducing-spring-boot)

1. Clone [this project](https://gitlab.com/hiring_nike_china/fetch-shoe-prices/) and start this nodejs server, following the instructions on this project. The project should run at port 9090
2. You can run `mvn dependency:tree` to list/install the maven dependencies used in this project.
3. To run the server (on port 8081), use this command: `mvn spring-boot:run`

APIs:

1. Get original price (randomly fetched) for a supplied shoe id:
```
URL (GET) - http://localhost:8081/api/shoe-price/1

Response:
{
    "shoePrice": 147
}
```

### Priority

The first priority task is to analysis the requirement of the task, consider both the normal use cases and corner cases. After that the second will be defining the JUnit test cases according to TDD principles. Then implement the code and do test. Once the code can run and be tested in local environment, I'll consider to make it to work in the Gitlab CICD pipeline

### Code Changes & Improvements

I create a folder for each type of classes: controller, service, dao, config, exception, and put the java class into the related folder, this can make the constructure clear. 
1. Created new class named Controller, all Restful API are exposed by this class, there are two methods(APIs) in it: 
getShoePrice(String id): comes from the original file, and I moved the code in it to ShoesDao.java. 
getFlatDiscount() returns all of the prices with 40 discount off.
2. Created class ShoesService to provide shoes services including the implementation of the new added API getFlatDiscount(). The Discount can be set by environment variable or java command line argument.
3. Created class ShoesDao, basicly does two things from data servers: one is to make restful call to http://localhost:9090/fetch-random-price/{id} to get random price and return to caller directly. The other is to load the prices of shoes from application.yml file, caculate and return to caller
4. Test cases added to src/test folder


### To Do List

If I have more time, I would like to implement two things: store the prices of shoes to database and read from DB to make it dynamically work, also an API to let admin user to modify the prices will be better

### Doubts and Assumptions

I assume the flat discount is base on the max prices of each pair of shoes.
